// console.log("Hello World");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function welcome(){
		let name = prompt("What is your name?");
		let age = prompt("How old are you?");
		let address = prompt("Where do you live?");
		alert("Thank you for your input");
		console.log("Hello, " + name);
		console.log("You are " + age + " years old.");
		console.log("You live in " + address);
	}

	welcome();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function top5Bands(){
		let top1Band = "1. The Beatles";
		let top2Band = "2. Metallica";
		let top3Band = "3. The Eagles";
		let top4Band = "4. L'arc∼en∼Ciel";
		let top5Band = "5. Eraserheads";
		console.log(top1Band);
		console.log(top2Band);
		console.log(top3Band);
		console.log(top4Band);
		console.log(top5Band);
	}

	top5Bands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function top5Movies(){
		let top1Movie = "1. The Godfather";
		let ratingtop1 = "97%";
		let top2Movie = "2. The Godfather, Part II";
		let ratingtop2 = "96%";
		let top3Movie = "3. Shawnshank Redemption";
		let ratingtop3 = "91%";
		let top4Movie = "4. To Kill A Mockingbird";
		let ratingtop4 = "93%";
		let top5Movie = "5. Psycho";
		let ratingtop5 = "96%";
		console.log(top1Movie);
		console.log("Rotten Tomatoes Rating: " + ratingtop1);
		console.log(top2Movie);
		console.log("Rotten Tomatoes Rating: " + ratingtop2);
		console.log(top3Movie);
		console.log("Rotten Tomatoes Rating: " + ratingtop3);
		console.log(top4Movie);
		console.log("Rotten Tomatoes Rating: " + ratingtop4);
		console.log(top5Movie);
		console.log("Rotten Tomatoes Rating: " + ratingtop5);
	}

	top5Movies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	// printUsers(); // error - printUsers() is not defined.
	let printFriends = function printUsers(){
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:");
		let friend2 = prompt("Enter your second friend's name:");
		let friend3 = prompt("Enter your third friend's name:");
		console.log("You are friends with:")
		console.log(friend1);
		console.log(friend2);
		console.log(friend3);
	};

	printFriends();

	// console.log(friend1); // error - only accessible on the function scope.
	// console.log(friend2); // error - only accessible on the function scope.